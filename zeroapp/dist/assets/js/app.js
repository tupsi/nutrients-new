
const s = success = "background-color:green;color:white;"
const w = warning = "background-color:yellow;color:black;"
const d = danger  = "background-color:red;color:white;"


const mzr = {
    "log":(loginfo, status)=>console.log("%c[nutrients]" +"%c"+` ${loginfo}`,status, '')
}
mzr.log("Ready",s)


async function registerSW(){
    if('serviceWorker' in navigator){
        try{
            await navigator.serviceWorker.register('/sw-zeroapp.js');
            mzr.log("Service worker is registered.",s)
        }catch(e){
            mzr.log("Service worker registration failed.",d)
        }
    }else{
        mzr.log("Your browser does not support Service Worker",w)
    }
}

window.addEventListener('load', e => {
    registerSW();
});