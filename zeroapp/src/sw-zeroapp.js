const s = success = "background-color:green;color:white;"
const w = warning = "background-color:yellow;color:black;"
const d = danger  = "background-color:red;color:white;"


const mzr = {
    "log":(loginfo, status)=>console.log("%c[nutrients]" +"%c"+` ${loginfo}`,status, '')
}

const cacheName = "nutrients-zeroapp-pwa-conf-v1";
const staticAssets = [
    './',
    './index.html',
    './assets/js/app.js',
    './assets/css/style.css',
    './assets/img/avatar.png',
    './assets/icons/n1/apple-touch-icon.png',
    './assets/icons/n1/favicon-32x32.png',
    './assets/icons/n1/favicon-16x16.png',
    './assets/icons/n1/favicon.png'
];

const cacheFirst = async (request)=>{
    const cache = await caches.open(cacheName);
    const cachedResponse = await cache.match(request);
    return cachedResponse || networkFirst(request);

}

const networkFirst = async (request)=>{
    const cache = await caches.open(request);

    try{
        const fresh = await fetch(request);
        cache.put(request, fresh.clone());
        return fresh;
    }catch(e){
        const cachedResponse = await cache.match(request);
        return cachedResponse;
    }
}

self.addEventListener('install', async event=>{
         const cache = await caches.open(cacheName);
        await cache.addAll(staticAssets);
});

self.addEventListener('fetch', async event=>{
    
    const request = event.request;
    
    if(/.*(json)$/.test(event.request.url)){
        event.respondWith(networkFirst(request));
    }else{
        event.respondWith(cacheFirst(request));
    }
    // mzr.log('fetch event', s);
});