## nutrients


*   master brach contains Zero App

*   other branches has its own variants using different technologies

*   nutrients will be the ultimate code generator

## Steps for manual PWA implementation

[1] Adding a web app manifest
[2] Registering Service Worker
[3] Using Service Worker : caching static assets
[4] Using Service Worker : serving static assets from cache
[5] Using Service Worker : Understand Strategy : Serving Dynamic Content. Dynamic Cache.

# Shortcomings of this approach

* We need to rememnerto update timestamp of service worker anytime weupdate our static resources
* Ther is no wau to update only changed static resources to save bandwidth
* Dynamic caching never expires and can potentially grow large
* Our initial caching can easily failif a network errors cause a single file fail


# npm command
* npm init
* npm install
* npm install http-server
* hot reloading???


# TODO

* Generator for app manifest
* Generator for icon